<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('home');
});

Route::get('/register', 'HomeController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/items', function() {
    return view('items.index');
});

Route::get('/data-table', function() {
    return view('items.data-table');
});

Route::get('/table', function () {
    return view('items.table');
});